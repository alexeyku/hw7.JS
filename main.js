function excludeBy(peopleList,exclude,name){
	let newArr = [];
	for (let i = 0; i < peopleList.length; i++) {
		for(let j = 0;j < exclude.length; j++){
			if (peopleList[i][name] !== exclude[j][name]) {
	  			newArr.push(peopleList[i]);
	 		}
		}
	}
	return newArr;
}

excludeBy(
	[{name: "Jho", role: "user", age: 22},
	{name: "Dany", role: "admin", age: 32},
	{name: "Tom", role: "user", age: 23}], 

	[{name: "Tom", role: "user", age: 23}],
	'role');
